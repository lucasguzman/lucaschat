// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
import firebase from "firebase/app";

// Add the Firebase services that you want to use
import "firebase/auth";
import "firebase/database";


  const firebaseConfig = {
    apiKey: "AIzaSyCSiB-w_tTfQHe7iIV_OQVLrAmTFLGzGUk",
    authDomain: "lucaschat-45ea5.firebaseapp.com",
    projectId: "lucaschat-45ea5",
    storageBucket: "lucaschat-45ea5.appspot.com",
    messagingSenderId: "669919807302",
    appId: "1:669919807302:web:7fd4db5eb0a4954881c747",
    databaseURL: "https://lucaschat-45ea5-default-rtdb.firebaseio.com/",
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  var firebaseAuth = firebase.auth();
  var firebaseDb = firebase.database();

  export { firebaseAuth, firebaseDb };
